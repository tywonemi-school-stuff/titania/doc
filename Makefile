BAD_SVGS != find sources -name '*.drawio.svg'
PNGS != echo $(BAD_SVGS) | sed -e "s/\.drawio\.svg/_conv\.png/g"
TIKZ != find sources -name '*.tex'
TIKZ_PDFS != echo $(TIKZ) | sed -e "s/\.tex/\_conv\.pdf/g"

all: doc.pdf

doc.pdf: analysis.md bibliography.bib $(PNGS) $(TIKZ_PDFS) sources/*.svg templates/ctuthesis.tex
	pandoc analysis.md \
	-s \
	--from markdown+auto_identifiers \
	--filter pandoc-minted/pandoc-minted.py \
	--filter pandoc-svg/pandoc-svg.py \
	--filter pandoc-eqnos \
	--filter pandoc-crossref \
	--top-level-division=chapter \
	--template=ctuthesis.tex \
	--bibliography bibliography.bib \
	--citeproc \
	--csl ieee.csl \
	--data-dir=. \
	--pdf-engine=latexmk \
	--pdf-engine-opt=-outdir=temp \
	--pdf-engine-opt=-shell-escape \
	-o doc.pdf

%_conv.png: %.drawio.svg
	convert -density 1200 -resize 2000 $< $@

sources/%_conv.pdf: sources/%.tex
	latexmk $< -pdf -outdir=temp -jobname=$(basename $(notdir $@))
	cp temp/$(notdir $@) $@