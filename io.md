https://www.rfsoc-pynq.io/pdf/RFSoC_2x2_UG.pdf

DDR4 PL
2x2 banks 67-69
DDR4 PS
2x2 PS GTR 504
analog
DAC
2x2 228, 229
ADC
2x2 224, 226 channel 1
x410 224, 226 channels 0,1
ETH
2x2 PHY: DP83867

clocking
2x2 Si5340 generates 200 MHz for PL DDR4 and something else for PS stuff (33.33MHz "processor clk")
2x2 LMK04832 generates through LMX2594 the ADC and DAC clock

