# GNU Radio
GNU Radio is free and open source software for signal processing and streaming samples to and from SDRs. Its predecessor SpectrumWare (//TODO cite 10.1145/215530.215551) has been created in the 1990s to aid in developing systems which process digitized baseband samples on general purpose personal computers with the goal of easily modifying the processing with a software update, and rapid prototyping. Conventional radio systems of the time had fixed functionality due to hardware implementation with limited configurability.

## Terms

### Xilinx-specific

### Other

//TODO accumulate all abbreviations and shorthands

//TODO unify "the" around terminology like RFNoC

## Background

# RFNoC

RFNoC (Radio Frequency Network on Chip) is in a step backwards from the absolute immediate reconfigurability of GNU Radio achieved by doing all signal processing in pure software. Instead of being fully done on a PC, RFNoC allows a developer to offload processing to programmable logic built into a SDR device. The merits of this approach are discussed below. RFNoC is an open specification ([RFNoC spec](https://files.ettus.com/app_notes/RFNoC_Specification.pdf)) with an open-source implementation, discussed later in the gateware section (//TODO reference).

## Usecase

### Latency

In wireless protocols, latency in responding to incoming packets might significantly harm throughput, routing, etc (//TODO. cite?). Latency of a system (up to USRP 3) with PC in the loop has several milliseconds of latency, comprised of the following:

+ SDR FPGA to SDR kernel
+ SDR kernel to transport bus (ethernet, USB)
+ transport bus to host PC kernel
+ PC kernel to GNU Radio application

Each of these is buffered to some extent. Data flowing through the operating system kernels is non-deterministic in latency. This may be insufficient for various low latency applications.

### Throughput

Latest RFSoC UltraScale+ systems on chip from Xilinx can sample at several GSps. Even though 100Gbps QSFP+ ports are present on the USRP X410, processing such a large dataflow can be very processing intensive. Bottlenecks can arise in kernel drivers and I/O. Hardware supporting 100G QSFP+ may also be impractical for deployment in embedded setups.

### Power

In general, hardware accelerators can use lower overhead and parallel processing to significantly reduce the total energy needed per unit of computation, for example, a single N-point FFT (//TODO cite [AN531: Reducing Power with Hardware Accelerators](https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/an/an531.pdf)).

## Openness

# USRP x410

## Software

All software to run on the USRP x4xx device is distributed as a yocto project. Yocto is a system for combining software repositories as "layers" which can be combined and patched into final software packages. It is similar in goal and function to git submodules. Bitbake is the build system used in yocto, and is comparable to make. A configuration utility from Siemens called kas automates the configuration of the layer to target the various USRP devices. Build-time requirement and dependency versions are frozen by invoking kas from the kas docker container. Practically no sources made outside of Ettus Research or National Instruments are included in the meta-ettus git repository, instead, kas/yocto pulls all of them as needed, and patches them to work on target devices.

To build it for x4xx, assuming the kas container is located in ~/kas, run in the top directory: `~/kas/kas-container --repo-rw build ./kas/x4xx.yml` //TODO retest on clean system - virtual machine? and edit after patches

Building the filesystem with kas uses the following layers, excluding the "meta-" name prefix:

+ ettus-core. This adds among other things the UHD software. //TODO review

+ titanium. Titanium is the codename for USRP x4xx. This layer adds features specific to the device. This includes bootloader (Das U-Boot), kernel (mainline linux 5.10), linux-firmware (Chromium EC for the SCU)

+ mender. Third-party cloud-based remote device management software with self-hosted options. This enables to upload new filesystem versions to the USRPs

+ openAMP. Open asynchronous multiprocessing. Implements communication and synchronization between SoC ARM cores (UG1186). Remoteproc and RPMsg are upstream linux kernel drivers for OpenAMP. Libmetal implements interfaces for OpenAMP on specific devices including Xilinx SoCs

+ xilinx. Xilinx non-linux software. This includes the PMU firmware and FSBL, first-stage bootloader. FSBL is unused in this project, replaced by Das U-Boot SPL

The yocto setup builds the following artifacts:

+ PMU firmware

+ Chromium EC for the STM32 SCU

+ GNU Radio image, which is a fully built linux filesystem, including the boot partition (?//TODO verify)

Configurations in meta-titanium/conf/ need new device tree blobs (.dtb, .dtbo) listed. These are built from device tree sources (.dts), that are listed in the patch 0044 in meta-titanium/recipes-kernel/linux/files/.

A flattened device tree (FDT) is a device tree with hierarchical information removed, analogous to the linking process of a program. The built image is stored and loaded as a flattened uImage tree (FIT). uImage is a simple image wrapper format for loading by the u-boot bootloader. FIT maps images (kernel, ramdisk, device tree blob) into memory and pairs them with configurations (maps together kernel, fdt, ramdisk) and hashes. The associated FIT filename extensions are .its for source files, and .itb for built binaries. The kernel loads its own FDT DTB and, as a result, requires FDT support enabled.

Documentation of u-boot image formats: https://github.com/lentinj/u-boot/blob/master/doc/uImage.FIT/

The process of modifying bitbake patches is as follows:

+ `devtool modify recipename` pulls recipe sources
+ you make your changes
+ commit your changes (no need to have access to push)
+ `devtool update-recipe recipename` creates patches and creates or extends `.bbappend` from your modifications

A recipe is defined by a `.bb` file. Recipename must be the name, not filename of the recipe, so exclude the version and `.bb` extension. For example, u-boot is pulled with `devtool modify u-boot`. These commands are most easily run within the `kas` container: `path/to/kas-container shell kas/x4xx.yml -c 'devtool modify u-boot'`. `kas-container` is a script within the root directory of the kas repository and requires having the docker built according to kas documentation.

Creating a layer: `bitbake-layers create-layer meta-titania'` https://www.yoctoproject.org/docs/current/bsp-guide/bsp-guide.html#creating-a-new-bsp-layer-using-the-bitbake-layers-script

Creating a BSP layer: https://www.yoctoproject.org/docs/3.1/bsp-guide/bsp-guide.html#developing-a-board-support-package-bsp

Patching described here: https://wiki.yoctoproject.org/wiki/TipsAndTricks/Patching_the_source_for_a_recipe

Emitting the PM config object for u-boot SPL: https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841724/PMU+Firmware

The PMU config is emitted by Vivado already.

What gets included from where into u-boot and kernel device trees?

### USRP Hardware Driver

The UHD repository includes all custom software by Ettus/NI:

+ firmware - designated for embedded devices other than the main SoC. This is unused with x400, since the STM32 microcontroller runs Chromium EC, which is pulled by yocto from its own repository, https://github.com/EttusResearch/usrp-firmware/
+ fpga - includes all sources for building the FPGA bitstream. This is discussed elsewhere (//TODO reference section)
+ host - software for controlling SDR hardware over the network or locally, to be run on devices that run the GNU Radio software, which typically is not the usecase for the SDR itself. Nevertheless it is built and included in the filesystem. Also includes generators for verilog sources for RFNoC core and blocks
+ images - utilities to download pre-built bitstream and device tree images
+ mpm - module peripheral manager is software that runs on the SDRs and interacts with the available hardware. This realizes the
    + RPC (remote procedure call) communication with the GNU Radio support implemented in the host directory
    + calibrates the ADCs
    + sets up IP addresses for ethernet ports on startup
    + sets network buffer sizes on startup
    + updates the filesystem when requested
    + access EEPROM storage - not present on x400

MPM installation creates systemd services for RPC with a hardware daemon named mpmd. These rely on installed host software.

The meta-ettus layer pulls these sources or pre-built artifacts (debian packages, FPGA bitstream) as a part of its build process.

### Build

//TODO kas container invocation

### Boot procedure

The boot procedure differs significantly from the conventional one.

As designed and documented by Xilinx, a Zynq MPSoC/RFSoC PMU runs from ROM, prepares the system monitor, configures power domains, clears PMU and CSU RAM and resets registers, and starts the CSU.
CSU also runs from a ROM, prepares SRAM, loads FSBL to it, and starts up an APU core. It also loads the PMU firmware to SRAM.
The APU executes the FSBL. The FSBL configures Zynq peripherals (ethernet, DDR RAM) and loads the configuration object for the PMU FW. It also loads the ARM Trusted Firmware and U-Boot, which are executed in sequence.
U-Boot loads its simplified device tree, loads the Linux kernel image into memory, and passes it the Linux device tree.
The PMU firmware configuration object consists of processing core access and power configuration variables.
//TODO cite CERN paper https://cds.cern.ch/record/2763095/files/CERN-THESIS-2021-031.pdf

The official approach as described above uses a fork of U-Boot. U-Boot has a FSBL replacement, the U-Boot SPL. For this reason, FSBL is not present or needed.
//TODO cite U-Boot SPL on ZynqMP article https://lucaceresoli.net/zynqmp-uboot-spl-pmufw-cfg-load/

Considering the scope of configuration the PMU FW performs is narrow, patching the device involves only modifying values configured by the U-Boot SPL. Because most of these (DDR timings, MIO signal routing for Ethernet) are tied directly to board hardware, these source files (most notably, `psu_init_gpl.c`) are emitted by Vivado by sourcing the `x4xx_rfdc_ps_bd.tcl` block diagram script with the modified `bdedit` Makefile target in gateware, and exporting hardware. This also exports a `.hdf` PetaLinux SDK file, which this project doesn't use.

The reader is warned that the official Xilinx documentation is in conflict with reality with the feasibility of the "community" boot procedure, claiming this is not possible for multiprocessing Zynq targets such as the RFSoC.
//TODO cite xilinx docs https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842574/U-Boot+Secondary+Program+Loader


## Gateware

All gateware specific to the x400 is present in the UHD repository, at the path `fpga/usrp3/top/x400`, other IP at `fpga/usrp3/lib`.

All Xilinx IP used in the RFSoC 2x2 port is available for free with a full Vivado license. Even though the paid "10G Ethernet Physical Coding Sublayer/Physical Medium Attachment (PCS/PMA)" IP core by Xilinx is referenced in the project, and generates warnings when built, it is not relevant for the RFSoC 2x2 board, as no QSFP port is present.

The following x4xx-specific Xilinx IP is used:

+ FIR Compiler, which emits FIR half-band filter VHDL for sample downconversion in the 200MHz ADC designs

+ AXI utilities: AXI Register Slice, FIFO Generator, AXI Interconnect and SmartConnect crossbars

+ DMA engine: AXI DMA with scatter gather functionality, used to read samples from PS DDR RAM to the RFNoC

RFNoC additionally uses the following Xilinx IP:

+ Fast Fourier Transform, attached to AXI

+ Complex Multiplier and CORDIC for manipulating complex samples

+ DDS Compiler (direct digital synthesis)

+ Divider Generator (integer only division)

The rest of the gateware project is implemented as hierarchical block diagram designs created in Vivado GUI. They compose of RTL sources mostly in Verilog, SystemVerilog, and IP blocks.

Gateware implementing the RFNoC blocks is located in `fpga/usrp3/lib/rfnoc`. When a change to the RFNoC flow graph is desired, a YAML configuration file in the device's top directory is modified to describe the endpoints and blocks available. To generate Verilog sources from the configuration file, the `rfnoc_image_builder` utility from `host/utils` is used. The Verilog source generation procedure uses Mako templates, a template library written in Python. These templates have the file extension `.v.mako` and are located in `host/python/uhd/imgbuilder/templates`.

When an RFNoC flow graph is changed to modify endpoints and blocks, only the core and routing gateware is modified. Invocation for regenerating the RFNoC core, after installing host tools on development machine (`cmake -G Ninja` and `sudo ninja install`) is as follows:

```bash
rfnoc_image_builder --generate-only -y x410_400_rfnoc_image_core.yml --fpga-dir ./
```

### Build

//TODO my vivado container invocation including version, license

Edit block design for example for the x4xx_ps_rfdc_bd IP:

```
make bdedit TARGET=x4xx_ps_rfdc_bd
```

### RF Data Converter

The ADC and DAC Zynq peripherals are configured for 3 GSps, though the ADC could be used up to 4.096 GSps, and DAC at up to 6.554 GSps. This is a reasonable choice considering the 400 MHz analog frontend. The bandwidth is further reduced to 200 MHz in the officially supported FPGA images. At the time of writing, 400 MHz gateware builds are considered experimental.

Each converter gets its own differential clock at a fixed ball of the FPGA package, much like the ADC inputs and DAC outputs. There is also a dedicated single SYSREF input signal for the entire RFDC subsystem. This is used to synchronize PLL and clock divider startup latency, and FIFO read/write pointer release latency. It does not handle sample clock synchronization, as this is unrealizable in silicon, and is a requirement for clock source quality and configuration as well as PCB design.
As an alternative to using directly the SYSREF from an external clock source, a single PL SYSREF input is available to be distributed between all ADCs, and another for all DACs.
The x410 as well as the Pynq use the dedicated SYSREF pin.

TODO cite https://support.xilinx.com/s/article/1071111?language=en_US

### Processing Subsystem

### SCU

On the x410, the SCU (system control unit) is a microcontroller running a bare metal control stack. It controls the power sequencing, the fan speeds, connects various peripherals and sensors to the Linux kernel, and performs other low-level tasks. This functionality is implemented within the Chromium EC (Embedded Controller) embedded framework. This framework has been developed by Google providing a communication interface with on-board microcontrollers for easy firmware updates and board peripheral management.

This hardware is not present on RFNoC 2x2.

## Disadvantages

### Licensing

Until USRP x4xx, 
No schematics since UHD 4, yes everything else, license 

### Time to build

+ 90 minute non-incremental bitstream

+ 90 minute incremental yocto, 40 GB build directory

### Complexity

TBD once whole setup is live and can be built from forks

## Provided blocks

## Creating blocks

Thanks to the existing infrastructure and documentation for testing, designing and including custom blocks is easier than starting from scratch without the RFNoC. This has great potential to accelerate modifications by communications developers with moderate digital design experience without requiring mastery of build systems and testing procedures.

Verilog Mako templates in `host/python/uhd/imgbuilder/templates` include a generic template for any RFNoC block, which is only used when new RFNoC blocks are created, and the generated source file is then filled in with logic written by the developer, and placed in `fpga/usrp3/lib/rfnoc/blocks/`. This procedure is further described in [Getting Started with RFNoC in UHD 4.0](https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0#Creating_Your_Own_RFNoC_Block).

# Linux

## Hardware differences between USRP x410 and Pynq RFSoC 2x2

+ clock sources slightly differ
+ only 2 ADC, 2 DAC
+ bandwidth not limited by frontend
+ no RF frontends are present nor are they needed for optical communications
+ PS pinout mostly kept the same due to fixed pin assignments
+ no QSFP ports for high bandwidth sample streaming
+ 4GB DDR4 RAM rather than 8GB is connected to PL. However, USRP x410 does not use the on-board PL RAM at the time of writing.

TODO link UG1085 UltraScale+ TRM
TODO link UG1137 MPSoC Software Developer Guide
TODO link PG201 MPSoC Processing System Product GUIDE

![](./pcb_clock.svg)

## Resulting u-boot modifications


## Resulting linux modifications

TODO:
+ u-boot device tree
+ linux device tree
+ device: MPM
+ host: UHD

## Resulting gateware modifications

DONE:
+ fix buses and RFNoC to match missing dboards, QSFP
+ fix PS DDR RAM to match hardware in MPSoC PS IP

TODO:

Since the reduction from 2x2 to 4x4 RF data converter configuration, the internal bus architecture in the PL had to be modified at multiple points.
As previously explained, the 


QSFP and CPLD related HDL files were removed from compilation.

In HDL designs, FPGA pin assignments, settings, and timing requirements are defined in constraint files. Xilinx software tools use the TCL-based XDC format.
These pin assignments are defined in the constraints subdirectory of the UHD FPGA design directory corresponding to the device (pynq).

In common.xdc, the QSFP interface and CPLD SPI were removed.

While the PL DRAM differs, it is unused in the original design, so the constraints in dram.xdc were left to be inconsistent
with Pynq's board design.

In rfdc_2x2.xdc, ADC and DAC reference clocks and RF I/O were remapped and reduced to match Pynq's 2x2 configuration.
Since these correspond to hard IP blocks in silicon, they cannot be remapped arbitrarily. Similarly, the behavior
cannot be altered by writing gateware, as the RF data converters' HDL interfaces are instantiated in automatically
generated gateware. What defines this instantiation, and the internal register settings of these converters,
are the parameters set for the RFDC IP in the block design (BD) editor, stored as x4xx_ps_rfdc_bd.tcl.
Since Vivado provides some verification functionality when it's used from the BD editor, the TCL config file
should not be edited by hand. Nevertheless, it is practical to refer to it to see the difference created by
patches and commits. Other than disabling unused channels, the notable setting changed here is to set the link
coupling to DC to ensure the converters can operate as close to 0 Hz as physically possible, as this is desirable
for optical communications baseband modulations.

The PS IP is configured in the same file. Here, the pinouts for
ethernet RGMII, SDIO, USB, eMMC, and low speed serial buses required modification,
as well as PS DRAM pinout and timings, as this is crucial for boot and basic functionality.

In order for Linux device drivers to use the peripherals made available through PL IP and hard IP configuration,
they need to be made aware of them and their parameters through the device tree. For this reason,
part of the FPGA build is the device-specific selection of device tree source includes from the dts directory.
These had to be modified as well: x410-common.dtsi had CPLD mainboard registers removed, and the top level .dts file
had QSFP including lines cut.


TODO DTSI

## Issues in gateware build

`One or more IPs have been locked in the design 'x4xx_ps_rfdc_bd.bd'. Please run report_ip_status for more details and recommendations on how to fix this issue.`

Resolution: 

